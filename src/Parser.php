<?php

namespace olexandrp\parser;

/**
 * @author Victor Zinchenko <zinchenko.us@gmail.com>
 * @edited by O.Petrenko <petrenko.inbox@gmail.com>
 */
class Parser implements ParserInterface {

    public function process(string $url, string $tag): array {

        $input = file_get_contents($url);


        preg_match_all("$tag", $input, $elements, PREG_SET_ORDER);

        foreach ($elements as $element) {
            echo '<pre>';
            print_r($element[1]);
            echo '</pre>';
        }


        return array();
    }

}

$parse = new Parser;

$url = 'http://news.liga.net/';
$tag = '/<div class=\"title\">(.*)<\/div>/iU';
$parse->process($url, $tag);
